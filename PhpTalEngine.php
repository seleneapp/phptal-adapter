<?php

/**
 * This File is part of the Selene\Adapters\PhpTal package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapters\PhpTal;

use \PHPTAL;
use \Selene\Components\View\Template\EngineInterface;
use \Selene\Components\View\Template\ResolverInterface;

/**
 * @class TwigEngine implements EngineInterface
 * @see EngineInterface
 *
 * @package Selene\Adapters\Twig
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class PhpTalEngine implements EngineInterface
{
    private $env;

    /**
     * templateResolver
     *
     * @var mixed
     */
    private $templateResolver;

    /**
     * @param TwigEnvironment  $env
     * @param ResolverInterface $templateResolver
     */
    public function __construct(PhpTalEnvironment $env, ResolverInterface $templateResolver)
    {
        $this->env = $env;
        $this->templateResolver = $templateResolver;
    }

    /**
     * render
     *
     * @param mixed $view
     * @param mixed $context
     *
     * @access public
     * @return string
     */
    public function render($template, array $context = [])
    {
        return $this->load($template)->render($context);
    }

    /**
     * supports
     *
     * @param mixed $extension
     *
     * @access public
     * @return boolean
     */
    public function supports($name)
    {
        return $name instanceof PHPTAL ? true : 'xhtml' === $this->templateResolver->resolve($name)->getEngine();
    }

    /**
     * load
     *
     * @param mixed $template
     *
     * @access protected
     * @return mixed
     */
    protected function load($template)
    {
        return $this->env->loadTemplate((string)$template);
    }
}
