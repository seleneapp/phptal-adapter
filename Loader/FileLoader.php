<?php

/**
 * This File is part of the Selene\Adapters\PhpTal package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapters\PhpTal\Loader;

use \Selene\Components\View\Template\LoaderInterface;
use \Selene\Components\View\Template\ResolverInterface;

/**
 * @class FileLoader
 * @package Selene\Adapters\PhpTal
 * @version $Id$
 */
class FileLoader implements LoaderInterface
{
    private $resolver;

    public function __construct(ResolverInterface $resolver)
    {
        $this->resolver = $resolver;
    }

    public function load($template)
    {
        return $this->resolver->resolve($template);
    }

    public function isValid($name, $time)
    {
        return filemtime($this->findTemplate($name)) <= $time;
    }
}
