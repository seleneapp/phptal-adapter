<?php

/**
 * This File is part of the Selene\Adapters\PhpTal package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapters\PhpTal;

use \Selene\Components\View\Template\LoaderInterface;

/**
 * @class PhpTalEnvironment
 *
 * @package Selene\Adapters\PhpTal
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class PhpTalEnvironment
{
    private $typeCache;
    private $loader;

    private $renderer;

    public function __construct(\PHPTAL $renderer, LoaderInterface $loader)
    {
        $this->loader   = $loader;
        $this->renderer = $renderer;
        $this->typeCache = [];
    }

    /**
     * render
     *
     * @param array $context
     *
     * @return string
     */
    public function render(array $context)
    {
        $this->setTemplateContext($this->renderer, $context);

        return $this->renderer->execute();
    }

    /**
     * loadTemplate
     *
     * @param mixed $name
     *
     * @return self
     */
    public function loadTemplate($name)
    {
        $this->setOutPutMode($template = $this->loader->load($name));

        $this->renderer->setTemplate($template);

        return $this;
    }

    /**
     * setTemplateContext
     *
     * @param \PHPTAL $template
     * @param array $context
     *
     * @return void
     */
    private function setTemplateContext(\PHPTAL $template, array $context)
    {
        foreach ($context as $key => $value) {
            $template->set($key, $value);
        }
    }

    /**
     * setOutPutMode
     *
     * @param string $template
     *
     * @return void
     */
    private function setOutPutMode($template)
    {
        $type = $this->getTemplateType($template);
        $mode = null;

        switch ($type) {
            case 'xml':
                $mode = \PHPTAL::XML;
                break;
            case 'xhtml':
                $mode = \PHPTAL::XHTML;
                break;
            case 'html':
                $mode = \PHPTAL::HTML5;
                break;
        }

        if ($mode) {
            $this->renderer->setOutPutMode($mode);
        }
    }

    /**
     * getTemplateType
     *
     * @param string $template
     *
     * @return strign
     */
    private function getTemplateType($template)
    {
        if (isset($this->typeCache[$template])) {
            return $this->typeCache[$template];
        }

        return $this->typeCache[$template] = pathinfo($template, PATHINFO_EXTENSION);
    }
}
